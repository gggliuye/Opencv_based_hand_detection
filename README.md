# Opencv_based_hand_detection

#### Introduction
* stage.1 . Use Opencv method to detect and find the bone of hands
* stage.2 . As this method has constraints in environment, try to make a more various dataset based on this method.
* stage.3 . Further use Deep learning method to track hand

#### Run

* need opencv
* need tensorflow Keras

#### Stage 1
Use opencv method to detect and find the bone of hands (more results could be found in juypyter notebook in the project)

![run_result](hand_detection.gif)

![cv_show](images/cv_show.PNG)

#### Stage 2 
Fake data set for DL (more results could be found in juypyter notebook in the project)
![data_made](images/data_made.PNG)


#### Stage 3
* CNN method to tell if we have hand or not -> show the probability of having hand in the view (more results could be found in juypyter notebook in the project)
![detect](images/detect_show.PNG)

* Saliency map method
show the saliency of the detection. (more results could be found in juypyter notebook in the project)
![saliency_show](images/saliency_show.PNG)